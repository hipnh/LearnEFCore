﻿namespace LearnEFCore.Model
{
    public class Score
    {
        public int Id { get; set; }
        public float ScoreValue { get; set; }

        public int StudentId { get; set; }
        public virtual Student Student { get; set; }

        public int SubjectId { get; set; }
        public virtual Subject Subject { get; set; }
    }
}
