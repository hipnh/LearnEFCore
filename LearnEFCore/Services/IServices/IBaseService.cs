﻿using System.Linq.Expressions;

namespace LearnEFCore.Services.IServices
{
    public interface IBaseService<T>
    {
        Task<T> GetAsync(Expression<Func<T,bool>> filter = null, bool tracked = true);
        Task<List<T>> GetAllAsync(Expression<Func<T, bool>> filter = null);
        Task AddAsync(T entity);
        Task RemoveAsync(T entity);
        Task UpdateAsync(T entity);
    }
}
