﻿using LearnEFCore.Model;

namespace LearnEFCore.Services.IServices
{
    public interface IStudentService:IBaseService<Student>
    {
    }
}
