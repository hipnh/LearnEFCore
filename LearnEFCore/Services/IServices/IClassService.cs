﻿using LearnEFCore.Model;

namespace LearnEFCore.Services.IServices
{
    public interface IClassService:IBaseService<Class>
    {
        Task<List<Class>> GetAllStudentByIdAsync(int id);
    }
}
