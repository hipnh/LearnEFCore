﻿using LearnEFCore;
using LearnEFCore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace DataAccess.Repository
{
    public class BaseService<T> : IBaseService<T> where T : class
    {
        private readonly ApplicationDBContext _applicationDbContext;
        protected readonly DbSet<T> _db;
        public BaseService(ApplicationDBContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
            _db = _applicationDbContext.Set<T>();
        }
        public async Task AddAsync(T entity)
        {
            await _db.AddAsync(entity);

        }

        public async Task RemoveAsync(T entity)
        {
            _db.Remove(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            _db.Update(entity);
        }

        public async Task<T> GetAsync(Expression<Func<T, bool>>? filter = null, bool tracked = true)
        {
            IQueryable<T> query = _db;
            if (!tracked)
            {
                query = query.AsNoTracking();
            }
            if (filter != null)
            {
                return await query.FirstOrDefaultAsync(filter);
            }
            return await query.FirstOrDefaultAsync();
        }

        public async Task<List<T>> GetAllAsync(Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> query = _db;
            if (filter != null)
            {
                query.Where(filter);
            }
            return await query.ToListAsync();
        }
    }
}
