﻿using LearnEFCore;
using LearnEFCore.Model;
using LearnEFCore.Services.IServices;

namespace DataAccess.Repository
{
    public class StudentService : BaseService<Student>, IStudentService
    {
        public StudentService(ApplicationDBContext applicationDbContext) : base(applicationDbContext)
        {
        }
    }
}
