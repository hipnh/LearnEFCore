﻿using LearnEFCore;
using LearnEFCore.Model;
using LearnEFCore.Services.IServices;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class ClassService : BaseService<Class>, IClassService
    {
        public ClassService(ApplicationDBContext applicationDbContext) : base(applicationDbContext)
        {
        }
        public async Task<List<Class>> GetAllStudentByIdAsync(int id)
        {
            return await _db.Where(c => c.Id == id).Include(s=>s.Students).ToListAsync();
        }
    }
}